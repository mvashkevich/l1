﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tracing.TraceInfos;
using Tracing.TraceResults;
using System.Diagnostics;
using System.Collections.Concurrent;

namespace Tracing
{
    public class Tracer : ITracer
    {
        private const int CallerStackFrameIndex = 1;
        private TraceInfo _traceInfo;
        private TraceResult _traceResult;
        private Dictionary<int, int> _threadIDs;
        private static int _id = 0;
        private object _lock;
        public Tracer()
        {
            _traceResult = new TraceResult();
            _traceInfo = new TraceInfo(_traceResult);
            _threadIDs = new Dictionary<int, int>();
            _lock = new object();
        }
        public TraceResult GetTraceResult()
        {
            return _traceResult;
        }

        public void StartTrace()
        {
            int threadID = Thread.CurrentThread.ManagedThreadId;
            if (!_threadIDs.ContainsKey(threadID))
            {
                lock (_lock)
                {
                    int newID = ++_id;
                    _threadIDs.Add(threadID, newID);
                }
            }
            threadID = _threadIDs[threadID];
            StackFrame stackFrame = new StackTrace().GetFrame(CallerStackFrameIndex);
            var method = stackFrame.GetMethod();
            string methodName = method.Name;
            string className = method.DeclaringType.Name;
            _traceInfo.StartTracing(threadID, className, methodName);
        }

        public void StopTrace()
        {
            int threadID = _threadIDs[Thread.CurrentThread.ManagedThreadId];
            _traceInfo.StopTracing(threadID);
        }
    }
}
