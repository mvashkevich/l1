﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracing.TraceResults;

namespace Tracing.Serialization
{
    public interface ITraceResultSerializer
    {
        void Serialize(TraceResult traceResult, TextWriter writer);
    }
}
