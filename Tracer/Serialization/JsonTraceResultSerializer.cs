﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Tracing.TraceResults;

namespace Tracing.Serialization
{
    public class JsonTraceResultSerializer : ITraceResultSerializer
    {
        public void Serialize(TraceResult traceResult, TextWriter writer)
        {
            var options = new JsonSerializerOptions() { WriteIndented = true, IgnoreNullValues = true };
            writer.Write(JsonSerializer.Serialize(traceResult, options));
        }
    }
}
