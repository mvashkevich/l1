﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Tracing.TraceResults;

namespace Tracing.Serialization
{
    public class XmlTraceResultSerializer : ITraceResultSerializer
    {
        private XmlSerializer _serializer = new XmlSerializer(typeof(TraceResult));
        public void Serialize(TraceResult traceResult, TextWriter writer)
        {
            _serializer.Serialize(writer, traceResult);
        }
        public TraceResult Deserialize(TextReader reader)
        {
            return _serializer.Deserialize(reader) as TraceResult;
        }
    }
}
