﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Tracing.TraceResults
{
    public class ThreadTraceResult
    {
        [XmlAttribute("id"), JsonPropertyName("id")] public int ThreadID { get; set; }
        [XmlAttribute("time"), JsonPropertyName("time")] public long TotalTime { get; set; }
        [XmlElement("method"), JsonPropertyName("methods")] public List<MethodTraceResult> MethodTraceResults { get; set; }
        public ThreadTraceResult(int threadID)
        {
            ThreadID = threadID;
            MethodTraceResults = new List<MethodTraceResult>();
        }
        public ThreadTraceResult() { }

    }
}
