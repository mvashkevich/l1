﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Tracing.TraceResults
{
    public class MethodTraceResult
    {
        [XmlAttribute("name"), JsonPropertyName("name")] public string MethodName { get; set; }
        [XmlAttribute("time"), JsonPropertyName("time")] public long Time { get; set; }
        [XmlAttribute("class"), JsonPropertyName("class")] public string ClassName { get; set; }
        [XmlElement("method"), JsonPropertyName("methods")]
        public List<MethodTraceResult> InnerMethods { get; set; }
    }
}
