﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Tracing.TraceResults
{
    [XmlRoot("root")]
    public class TraceResult
    {
        [XmlElement("thread"), JsonPropertyName("threads")]
        public List<ThreadTraceResult> ThreadTraceResults { get; set; }
        public TraceResult()
        {
            ThreadTraceResults = new List<ThreadTraceResult>();
        }
    }
}
