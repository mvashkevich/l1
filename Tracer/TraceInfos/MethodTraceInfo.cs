﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Tracing.TraceResults;

namespace Tracing.TraceInfos
{
    class MethodTraceInfo
    {
        internal string _className;
        internal string _methodName;
        internal Stopwatch _stopwatch;
        internal MethodTraceResult _methodTraceResult;
        internal IList<MethodTraceInfo> _innerMethods;

        internal MethodTraceInfo(string className, string methodName, MethodTraceResult methodTraceResult)
        {
            _className = className;
            _methodName = methodName;
            _methodTraceResult = methodTraceResult;
            _stopwatch = Stopwatch.StartNew();
        }
    }
}
