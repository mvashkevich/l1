﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracing.TraceResults;

namespace Tracing.TraceInfos
{
    class TraceInfo
    {
        internal Dictionary<int, ThreadTraceInfo> _threadTraceInfos;
        internal TraceResult _traceResult;

        internal TraceInfo(TraceResult traceResult)
        {
            _threadTraceInfos = new Dictionary<int, ThreadTraceInfo>();
            _traceResult = traceResult;
        }
        internal void StartTracing(int threadID, string className, string methodName)
        {
            if (!_threadTraceInfos.ContainsKey(threadID))
            {
                var threadTraceResult = new ThreadTraceResult(threadID);
                _traceResult.ThreadTraceResults.Add(threadTraceResult);
                _threadTraceInfos.Add(threadID, new ThreadTraceInfo(threadID, threadTraceResult));
            }
            _threadTraceInfos[threadID].StartTracing(className, methodName);
        }

        internal void StopTracing(int threadID)
        {
            if (_threadTraceInfos.ContainsKey(threadID))
            {
                _threadTraceInfos[threadID].StopTracing();
            }
        }
    }
}
