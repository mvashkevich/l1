﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracing.TraceResults;

namespace Tracing.TraceInfos
{
    internal class ThreadTraceInfo
    {
        internal int _threadID;
        internal long _totalTime;
        internal Stack<MethodTraceInfo> _traceStack;
        internal ThreadTraceResult _threadTraceResult;

        internal ThreadTraceInfo(int threadID, ThreadTraceResult threadTraceResult)
        {
            _threadID = threadID;
            _traceStack = new Stack<MethodTraceInfo>();
            _threadTraceResult = threadTraceResult;
        }
        internal void StartTracing(string className, string methodName)
        {
            var methodTraceResult = new MethodTraceResult() { ClassName = className, MethodName = methodName };
            var methodTraceInfo = new MethodTraceInfo(className, methodName, methodTraceResult);
            _traceStack.Push(methodTraceInfo);
        }
        internal void StopTracing()
        {
            if (_traceStack.Count != 0)
            {
                var methodTraceInfo = _traceStack.Pop();
                methodTraceInfo._stopwatch.Stop();
                long time = methodTraceInfo._stopwatch.ElapsedMilliseconds;
                methodTraceInfo._methodTraceResult.Time = time;

                // inner method
                if (_traceStack.Count != 0)
                {
                    var outerMethodTraceInfo = _traceStack.Peek();
                    if (outerMethodTraceInfo._methodTraceResult.InnerMethods == null)
                    {
                        outerMethodTraceInfo._methodTraceResult.InnerMethods = new List<MethodTraceResult>();
                    }
                    outerMethodTraceInfo._methodTraceResult.InnerMethods.Add(methodTraceInfo._methodTraceResult);
                }
                // outer method
                else
                {
                    _threadTraceResult.MethodTraceResults.Add(methodTraceInfo._methodTraceResult);
                }
                _threadTraceResult.TotalTime += time;

            }
        }
    }
}
