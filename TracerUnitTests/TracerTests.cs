﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tracing;

namespace TracerUnitTests
{
    [TestClass]
    public class TracerTests
    {
        class TracingTest
        {
            internal const int SleepTime = 20;
            private ITracer _tracer;
            internal TracingTest(ITracer tracer)
            {
                _tracer = tracer;
            }
            public void FirstNestingMethodTracing1()
            {
                _tracer.StartTrace();
                Thread.Sleep(SleepTime);
                _tracer.StopTrace();
            }
            public void FirstNestingMethodTracing2()
            {
                _tracer.StartTrace();
                Thread.Sleep(SleepTime);
                _tracer.StopTrace();
            }
            public void SecondNestingMethodTracing()
            {
                _tracer.StartTrace();
                Thread.Sleep(SleepTime);
                FirstNestingMethodTracing1();
                FirstNestingMethodTracing2();
                _tracer.StopTrace();
            }
            public void ThirdNestingMethodTracing()
            {
                _tracer.StartTrace();
                Thread.Sleep(SleepTime);
                SecondNestingMethodTracing();
                _tracer.StopTrace();
            }
        }
        [TestMethod]
        public void TestNoTracing()
        {
            var tracer = new Tracer();
            var result = tracer.GetTraceResult();
            Assert.IsTrue(result.ThreadTraceResults.Count == 0, "TraceResult isn't empty");
        }
        [TestMethod]
        public void TestOneMethod()
        {
            var tracer = new Tracer();
            TracingTest tracingTest = new TracingTest(tracer);
            tracingTest.FirstNestingMethodTracing1();

            var result = tracer.GetTraceResult();
            Assert.IsTrue(result.ThreadTraceResults.Count == 1, "Number of threads is invalid");

            var threadTraceResult = result.ThreadTraceResults[0];
            Assert.IsTrue(
                threadTraceResult.TotalTime >= TracingTest.SleepTime,
                "Measured thread time is less than expected");
            Assert.IsTrue(threadTraceResult.MethodTraceResults.Count == 1,
                "Number of methods is invalid");

            var methodTraceResult = threadTraceResult.MethodTraceResults[0];
            Assert.AreEqual(methodTraceResult.ClassName, nameof(TracingTest),
                "Class name is invalid");
            Assert.AreEqual(methodTraceResult.MethodName, nameof(TracingTest.FirstNestingMethodTracing1),
                "Method name is invalid");
            Assert.IsNull(methodTraceResult.InnerMethods,
                "Method trace result shouldn't contain inner methods");
            Assert.IsTrue(methodTraceResult.Time >= TracingTest.SleepTime,
                "Measured method time is less than expected");
        }
        [TestMethod]
        public void TestTwoThreads()
        {
            var tracer = new Tracer();
            TracingTest tracingTest = new TracingTest(tracer);
            var secondThread = new Thread(tracingTest.ThirdNestingMethodTracing);
            secondThread.Start();
            tracingTest.ThirdNestingMethodTracing();
            secondThread.Join();

            const int expectedTotalTime = 4 * TracingTest.SleepTime;

            var result = tracer.GetTraceResult();
            Assert.AreEqual(result.ThreadTraceResults.Count, 2, "Invalid threads count");

            var firstThreadResult = result.ThreadTraceResults[0];
            Assert.IsTrue(firstThreadResult.TotalTime >= expectedTotalTime, "Invalid time in first thread");

            var secondThreadResult = result.ThreadTraceResults[1];
            Assert.IsTrue(secondThreadResult.TotalTime >= expectedTotalTime, "Invalid time in second thread");
        }
        [TestMethod]
        public void TestInnerMethods()
        {
            var tracer = new Tracer();
            TracingTest tracingTest = new TracingTest(tracer);
            tracingTest.ThirdNestingMethodTracing();

            var result = tracer.GetTraceResult();
            var threadTraceResult = result.ThreadTraceResults[0];
            const int expectedTotalTime = 4 * TracingTest.SleepTime;
            Assert.IsTrue(threadTraceResult.TotalTime >= expectedTotalTime,
                "Measured thread time is less than expected");

            var methodTraceResult = threadTraceResult.MethodTraceResults[0];
            Assert.IsTrue(methodTraceResult.Time >= expectedTotalTime,
                "Measured outer method time is less than expected");
            Assert.IsNotNull(methodTraceResult.InnerMethods,
                "Method trace result doesn't contain second nesting method trace info");

            var innerMethodTraceResult = methodTraceResult.InnerMethods[0];
            const int expectedSecondNestingTime = 3 * TracingTest.SleepTime;
            Assert.IsTrue(innerMethodTraceResult.Time >= expectedSecondNestingTime,
                "Second nesting measured time is less than expected");
            Assert.AreEqual(innerMethodTraceResult.MethodName, nameof(TracingTest.SecondNestingMethodTracing),
                "Second nesting method name is invalid");
            Assert.IsNotNull(innerMethodTraceResult.InnerMethods,
                "Second nesting trace result doesn't contain third nesting method trace info");
            Assert.AreEqual(innerMethodTraceResult.InnerMethods.Count, 2,
                "Second nesting trace doesn't contain info about two inner methods");

            var thirdNestingMethod1TraceResult = innerMethodTraceResult.InnerMethods[0];
            Assert.IsTrue(innerMethodTraceResult.Time >= expectedSecondNestingTime,
               "Third nesting measured time in first method is less than expected");
            
            var thirdNestingMethod2TraceResult = innerMethodTraceResult.InnerMethods[1];
            Assert.IsTrue(innerMethodTraceResult.Time >= expectedSecondNestingTime,
               "Third nesting measured time in second method is less than expected");
        }
    }
}
