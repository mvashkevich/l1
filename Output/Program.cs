﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tracing;
using Tracing.Serialization;
using Tracing.TraceResults;

namespace Output
{
    class Program
    {
        static void Main(string[] args)
        {
            Tracer tracer = new Tracer();
            Thread thread2 = new Thread(TestThread2);
            thread2.Start(tracer);
            Thread thread3 = new Thread(TestThread3);
            thread3.Start(tracer);
            TestThread1(tracer);
            thread2.Join();
            thread3.Join();
            var result = tracer.GetTraceResult();

            var xmlSerializer = new XmlTraceResultSerializer();
            var jsonSerializer = new JsonTraceResultSerializer();
            Console.WriteLine("Xml:");
            Writer.Write(Console.Out, xmlSerializer, result);
            Console.WriteLine("\nJson:");
            Writer.Write(Console.Out, jsonSerializer, result);
            using (var s = new StreamWriter("traceResults.xml"))
                Writer.Write(s, xmlSerializer, result);
            using (var s = new StreamWriter("traceResults.json"))
                Writer.Write(s, jsonSerializer, result);

            //using (var r = new StreamReader("out.txt"))
            //{
            //    result = xmlSerializer.Deserialize(r);
            //}

            //Writer.Write(Console.Out, xmlSerializer, result);
            //Writer.Write(Console.Out, jsonSerializer, result);
            Console.ReadLine();

        }
        static void TestThread1(object argument)
        {
            if (argument is ITracer)
            {
                var tracer = argument as ITracer;
                InnerTest inner = new InnerTest(tracer);
                OuterTest outer = new OuterTest(tracer, inner);
                inner.InnerMethod1();
                inner.InnerMethod2();
                outer.OuterMethod1();
                outer.OuterMethod2();
            }
        }
        static void TestThread2(object argument)
        {
            if (argument is ITracer)
            {
                var tracer = argument as ITracer;
                InnerTest inner = new InnerTest(tracer);
                OuterTest outer = new OuterTest(tracer, inner);
                outer.OuterMethod3();
            }
        }
        static void TestThread3(object argument)
        {
            if (argument is ITracer)
            {
                var tracer = argument as ITracer;
                InnerTest inner = new InnerTest(tracer);
                OuterTest outer = new OuterTest(tracer, inner);
                outer.OuterMethod1();
                outer.OuterMethod2();
                outer.OuterMethod3();
            }
        }
    }
}
