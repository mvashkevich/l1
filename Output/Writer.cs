﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracing.Serialization;
using Tracing.TraceResults;

namespace Output
{
    class Writer
    {
        public static void Write(TextWriter writer, ITraceResultSerializer serializer, TraceResult traceResult)
        {
            serializer.Serialize(traceResult, writer);
        }
    }
}
