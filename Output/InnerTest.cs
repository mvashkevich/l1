﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tracing;

namespace Output
{
    class InnerTest
    {
        private const int DefaultSleepDuration = 50;
        private const int LongSleepDuration = 200;

        protected ITracer _tracer;

        public InnerTest(ITracer tracer)
        {
            _tracer = tracer;
        }

        public void InnerMethod1()
        {
            _tracer.StartTrace();
            Thread.Sleep(DefaultSleepDuration);
            _tracer.StopTrace();
        }

        public void InnerMethod2()
        {
            _tracer.StartTrace();
            Thread.Sleep(LongSleepDuration);
            _tracer.StopTrace();
        }
    }
}
