﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tracing;

namespace Output
{
    class OuterTest
    {
        private const int DefaultSleepDuration = 100;
        private const int LongSleepDuration = 200;

        protected ITracer _tracer;
        protected InnerTest _inner;
        public OuterTest(ITracer tracer, InnerTest inner)
        {
            _tracer = tracer;
            _inner = inner;
        }
        public void OuterMethod1()
        {
            _tracer.StartTrace();
            Thread.Sleep(DefaultSleepDuration);
            _inner.InnerMethod1();
            Thread.Sleep(DefaultSleepDuration);
            _tracer.StopTrace();
        }
        public void OuterMethod2()
        {
            _tracer.StartTrace();
            Thread.Sleep(LongSleepDuration);
            _inner.InnerMethod2();
            Thread.Sleep(DefaultSleepDuration);
            _tracer.StopTrace();
        }
        public void OuterMethod3()
        {
            _tracer.StartTrace();
            Thread.Sleep(DefaultSleepDuration);
            _inner.InnerMethod1();
            _inner.InnerMethod2();
            _tracer.StopTrace();
        }
    }
}
